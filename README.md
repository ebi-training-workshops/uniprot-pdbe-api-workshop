## Exploring Programmatic Access to Protein Sequence, Function and Structure Information

### Summary

Integrating publicly available biological data from multiple data sources (including your own) can be critical to analyse data and infer patterns that may not otherwise be obvious. In this workshop you will learn how to discover and integrate biological information as well as visualisation components from protein resources at the European Bioinformatics Institute (EMBL-EBI), with a focus on UniProt and PDBe. 

Learn about accessing data such as gene-protein relationships, protein sequences, protein structures and function. Explore web services, like the [UniProt API](https://www.ebi.ac.uk/proteins/api/doc/) or the [PDBe API]( http://www.ebi.ac.uk/pdbe/api/doc/) providing programmatic access to data as well as freely available visualisation components, such as [PDBe’s LiteMol viewer](https://www.ebi.ac.uk/pdbe/pdb-component-library/doc.html#a_LiteMol) and [UniProt’s ProtVista](http://ebi-uniprot.github.io/ProtVista/) that you can re-use to visualise this data as well as your own.
